#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>

__global__ void square(float * d_out, float * d_in, float * g_in){
	int idx = threadIdx.x;
	float f = d_in[idx];
	float g = g_in[idx];
	d_out[idx] = f + g;
}

int main(int argc, char ** argv){
	const int ARRAY_SIZE = 64;
	const int ARRAY_BYTES = ARRAY_SIZE * sizeof(float);

	// generate the input array on the host
	float h_in[ARRAY_SIZE];
	float g_in[ARRAY_SIZE];
	for (int i = 0; i < ARRAY_SIZE; i++){
		printf("%d :", i);
		h_in[i] = float(i);
		float f = ARRAY_SIZE - i;
		printf("%f \n", f);
		g_in[i] = f;
	}
	float h_out[ARRAY_SIZE];

	// declare GPU memory pointers
	float * d_in;
	float * dg_in;
	float * d_out;

	// allocate GPU memory
	cudaMalloc((void **)&d_in, ARRAY_BYTES);
	cudaMalloc((void **)&dg_in, ARRAY_BYTES);
	cudaMalloc((void **)&d_out, ARRAY_BYTES);

	// transfer the array to the GPU
	cudaMemcpy(d_in, h_in, ARRAY_BYTES, cudaMemcpyHostToDevice);
	cudaMemcpy(dg_in, g_in, ARRAY_BYTES, cudaMemcpyHostToDevice);


	// launch the kernel
	square << <1, ARRAY_SIZE >> >(d_out, d_in, dg_in);

	// copy back the result array to the CPU
	cudaMemcpy(h_out, d_out, ARRAY_BYTES, cudaMemcpyDeviceToHost);

	// print out the resulting array
	for (int i = 0; i< ARRAY_SIZE; i++){
		printf("%f \n", h_out[i]);
	}

	// free GPU memory allocation
	cudaFree(d_in);
	cudaFree(dg_in);
	cudaFree(d_out);

	getchar();
	return 0;

}
