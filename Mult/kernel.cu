#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <math.h>

#include <time.h>
#include <cuda.h>
#include <assert.h>

#define M 1
#define N 1
#define K 1
#define TITLE_Wi

__global__ void MultMatrix(float* m_A, float* m_B, float* m_C, int n, int m, int k){
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int col = blockIdx.x * blockDim.x + threadIdx.x;

	if ((row < m) && (col < k)){
		float elemento = 0.0;

		for (int i = 0; i < n; ++i)
			elemento += m_A[row*n + i] * m_B[col + i*k];
		m_C[row*k + col] = elemento;
	}
}

__host__ void vcMult(float m_A[][])