#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <math.h>

#include <time.h>
#include <cuda.h>
#include <assert.h>

#define M 1000
#define N 1000
#define K 1000
#define TILE_WIDTH 2

__global__ void MatMulKernel(float* d_A, float* d_B, float* d_C, int n, int m, int k) {
	// Each thread computes one element of C
	// by accumulating results into Cvalue

	  int row = blockIdx.y * blockDim.y + threadIdx.y;
	  int col = blockIdx.x * blockDim.x + threadIdx.x;
	if ((row < m) && (col < k)){
		float Cvalue = 0.0;

		for (int i = 0; i < n; ++i)
			Cvalue += d_A[row * n + i] * d_B[col + i*k];

		d_C[row * k + col] = Cvalue;
	}
}

void guardarMatriz(float Matrix[][M]){
	//Aqui esta localiza��o do arquivo
	char url[] = "Matriz.txt";
	
	//Defini��o do arquivo
	FILE *arq;
	//abrimos o arquivo de forma que ele escreva no final do arquivo sem apagalo
	arq = fopen(url, "a");
	if (arq == NULL)
		printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		fprintf(arq, "########## Matriz %d x %d ############\n", N, M);
		for (int i = 0; i < N; i++){
			for (int j = 0; j < M; j++){
				fprintf(arq, " %.0f", Matrix[i][j]);
			}
			fprintf(arq, "\n");
		}
		fprintf(arq, "\n \n");

		//fecha o fluxo do arquivo
		fclose(arq);
	}
}

void mostarMatrix(float Matrix[][M]){
	printf("########## Matriz %d x %d ############ \n", N, M);
	/*for (int i = 0; i < N; i++){
		for (int j = 0; j < M; j++){
			printf(" %.0f", Matrix[i][j]);
		}
		printf("\n");
	}
	printf("\n \n");*/
	
	guardarMatriz(Matrix);

}

__host__ void vecMult(float h_A[M][N], float h_B[M][N], float h_C[M][N], int n, int m, int k) {
	int size = n * m * sizeof(float);
	float *d_A; float *d_B; float *d_C;
	int tamanho = 32;
	dim3 DimGrid((n - 1) / tamanho + 1, (n - 1) / tamanho + 1, 1);
	dim3 DimBlock(tamanho, tamanho, 1);
	//declara�oes para contar tempo
	clock_t begin, end;
	double time_spent;
	begin = clock();

	//copia para memoria da gpu
	cudaMalloc((float **)&d_A, size);
	cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);
	cudaMalloc((float **)&d_B, size);
	cudaMemcpy(d_B, h_B, size, cudaMemcpyHostToDevice);
	cudaMalloc((float **)&d_C, size);
	
	//verifica��o de tempo at� aqui
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Tempo de copia para cuda: %f \n", time_spent);	

	//execu��o do codigo CUDA
	MatMulKernel << <DimGrid, DimBlock >> >(d_A, d_B, d_C, n, m, k);
	
	//verifica��o de tempo at� aqui
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Tempo de copia para cuda + execu��o: %f \n", time_spent);

	//copia para memoria da CPU
	cudaMemcpy(h_C, d_C, size, cudaMemcpyDeviceToHost);

	//verifica��o de tempo at� aqui
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Tempo de copia para CPU + finaliza��o: %f \n", time_spent);
	
	//libera��o da memoria da gpu
	cudaFree(d_A); cudaFree(d_B); cudaFree(d_C);
}

int main()
{

	const int size = N * N;
	const int size_bytes = size * sizeof(float);

	static float A_cpu[M][N], B_cpu[N][M], C_cpu[M][K];


	for (int i = 0; i < N; i++){
		for (int j = 0; j < N; j++){
			A_cpu[i][j] = 1;
			B_cpu[i][j] = 1;
		}
	}
	vecMult(A_cpu, B_cpu, C_cpu, N, M, K);

	//mostarMatrix(A_cpu);
	//mostarMatrix(B_cpu);
	mostarMatrix(C_cpu);

	getchar();
	return 0;
}
